var fs = require("fs");
var ifttt = require('./modules/ifttt');
var _ = require('lodash');

// Load all the items and send each one to IFTTT.
var stdin = process.stdin;
var stdout = process.stdout;

var inputChunks = [];

stdin.resume();
stdin.setEncoding('utf8');

stdin.on('data', function (chunk) {
  inputChunks.push(chunk);
});

stdin.on('end', function () {
  var items = JSON.parse(inputChunks.join(''));
  console.log(items);

  _.each(items.items, function (item) {
    ifttt.notify({
      value1: `${item.name} (${item.ranking})`,
      value2: item.link,
      value3: item.description
    })
  });
});
