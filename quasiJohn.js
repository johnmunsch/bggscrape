var exec = require('child_process').exec;
var CronJob = require('cron').CronJob;

var command = 'node getPageList.js | jq "[.[] | select(.pageNumber > 258)]" | node getItemsFromPages.js | jq "{ lastPage: .lastPage, lastItem: .lastItem, "items": [.items | .[] | select(.ranking != null) | select(.ranking < 1000)] }" | node filter.js | node notifyIFTTT.js';

new CronJob('0 8,22,33,38,51 * * * *', function() {
  var child = exec(command,
    { maxBuffer: 2000*1024 },
    function (error, stdout, stderr) {
      console.log('stdout: ' + stdout);
      console.log('stderr: ' + stderr);
      if (error !== null) {
        console.log('exec error: ' + error);
      }
  });
}, null, true);
