var fs = require("fs");
var Q = require('q');
var scraper = require('./modules/scraper');
var _ = require('lodash');

var bgg = {
  scope: 'div.mb5',
  selector: [{
    number: 'div.fl a:nth-child(1)',
    link: 'div.fl a:nth-child(1)@href',
    name: 'div.fl a:nth-child(2)',
    ranking: 'div.fl a:last-child',
    description: 'dd.doubleright@html'
  }],
  postProcessor: function (items) {
    return _.map(items, function (item) {
      item.number = _.parseInt(item.number);
      item.ranking = _.parseInt(item.ranking);

      return item;
    })
  }
};

// Load a set of pages to scrape and invoke the scraper for each page. Make a
// list of all the items which come back from the scraping.
var stdin = process.stdin;
var stdout = process.stdout;

var inputChunks = [];
var lastPage = -Infinity;
var lastItem = -Infinity;

stdin.resume();
stdin.setEncoding('utf8');

stdin.on('data', function (chunk) {
  inputChunks.push(chunk);
});

stdin.on('end', function () {
  var urls = JSON.parse(inputChunks.join(''));
  var allUrlPromises = [];

  _.each(urls, function (url) {
    // Keep track of the last page we scraped.
    if (url.pageNumber > lastPage) {
      lastPage = url.pageNumber;
    }

    allUrlPromises.push(scraper.scrapeSite(url.url, bgg));
  });

  Q.all(allUrlPromises).then(function (allItems) {
    allItems = _.flatten(allItems);
    var tempItem = _.max(allItems, 'number');

    if (tempItem) {
      lastItem = tempItem.number;
    }

    stdout.write(JSON.stringify({
      lastPage: lastPage,
      lastItem: lastItem,
      items: allItems
    }));
  });
});
